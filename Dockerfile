ARG FROM_IMAGE
FROM ${FROM_IMAGE}

# 설정 파라메터
ENV DEBIAN_FRONTEND noninteractive

# 기본 repo 업데이트
RUN add-apt-repository ppa:ubuntu-toolchain-r/ppa -y \
&&  add-apt-repository ppa:deadsnakes/ppa -y \
&&  apt update && mkdir -p /var/log/apt && apt install -y apt-utils
RUN apt install -y sudo gcc g++ make openjdk-11-jdk git wget curl vim net-tools rsync openssh-server \
procps dialog progress lsb-core gettext screen locales systemd systemd-sysv dbus-user-session kmod

# Locale 설정
RUN localedef -i ko_KR -f UTF-8 ko_KR.UTF-8
# locale-gen ko_KR.UTF-8
ENV LC_ALL ko_KR.UTF-8
ENV LANG ko_KR.UTF-8

# node.js 및 플러그인 설치
# RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.0/install.sh | bash
# RUN echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc && \
#     echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm' >> ~/.bashrc && \
#     echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion' >> ~/.bashrc && \
#     /bin/bash -c 'source $HOME/.bashrc' && \
#     nvm install 10.15.1 && \
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash \
&&  curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
&&  apt-get update && mkdir -p /var/log/apt \
&&  apt-get install -y nodejs yarn \
&&  npm install -g eslint configurable-http-proxy

# zsh 설정
RUN apt install -y zsh
RUN chsh -s /bin/zsh \
&&  sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" \
&&  git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions \
&&  sed -ie 's/^plugins=(/\0zsh-autosuggestions /' ${HOME}/.zshrc \
&&  exit

# NVIDIA 드라이버 지정 설치
# ENV NVIDIA_DRIVER_VERSION=470
# RUN mkdir -p /var/log/apt \
# &&  dpkg-statoverride --remove /usr/bin/crontab \
# &&  dpkg-statoverride --remove /usr/lib/dbus-1.0/dbus-daemon-launch-helper \
# &&  apt update && apt install -y nvidia-driver-${NVIDIA_DRIVER_VERSION} nvidia-utils-${NVIDIA_DRIVER_VERSION}

# CUDA 지정 설치 및 업데이트 (이미지 기본제공 버전: 11.0, 커스텀 버전 설치시 사용)
# RUN apt update && apt upgrade -y
# ENV CUDA_VERSION 11.2
# ENV CUDNN_VERSION 8.2.1.32
# RUN apt install --allow-downgrades --no-install-recommends -y \
# cuda-${CUDA_VERSION//./-} libcudnn${CUDNN_VERSION:0:1}=${CUDNN_VERSION}+cuda${CUDA_VERSION} libcudnn${CUDNN_VERSION:0:1}-dev=${CUDNN_VERSION}+cuda${CUDA_VERSION} \
# &&  update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-11.1 2 \
# &&  update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-11.2 3 \
# &&  echo 2 | update-alternatives --config cuda

# CUDA, CUDNN 환경변수 설정
RUN echo export 'CUDA_PATH="/usr/local/cuda"' >> /etc/profile \
&&  echo export 'PATH="${CUDA_PATH}/bin:${PATH}"' >> /etc/profile \
&&  echo export 'LD_LIBRARY_PATH="${CUDA_PATH}/lib64:${LD_LIBRARY_PATH}"' >> /etc/profile \
&&  echo export 'CUDA_PATH="/usr/local/cuda"' >> /etc/bashrc \
&&  echo export 'PATH="${CUDA_PATH}/bin:${PATH}"' >> /etc/bashrc \
&&  echo export 'LD_LIBRARY_PATH="${CUDA_PATH}/lib64:${LD_LIBRARY_PATH}"' >> /etc/bashrc

# (선택) APT로 파이썬 설치할 경우 (기본값 python3.8)
RUN apt install -y python3.8 python3.9
RUN update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1 \
&&  update-alternatives --install /usr/bin/python python /usr/bin/python3.8 2 \
&&  update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1 \
&&  update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 2 \
&&  update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 3 \
&&  echo 2 | update-alternatives --config python \
&&  echo 1 | update-alternatives --config python3
# RUN rm /usr/local/bin/pip && rm /usr/local/bin/pip3

RUN apt install -y python-pip python3-pip python3-distutils python3-distutils-extra python-six python3-six \
&&  python3.8 -m pip install -U six
RUN curl https://bootstrap.pypa.io/get-pip.py -o ./get-pip.py \
&&  python3.8 get-pip.py
RUN update-alternatives --install /usr/local/bin/pip pip /usr/local/bin/pip3.8 2 \
&&  update-alternatives --install /usr/local/bin/pip3 pip3 /usr/local/bin/pip3.6 1 \
&&  update-alternatives --install /usr/local/bin/pip3 pip3 /usr/local/bin/pip3.8 2 \
&&  echo 2 | update-alternatives --config pip \
&&  echo 1 | update-alternatives --config pip3

# pip 및 초기 라이브러리 설치 
ADD ./requirements.txt requirements.txt
RUN pip install -U pip && pip install -U -r requirements.txt
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager && jupyter lab build

# (선택) PYENV 설치하는 경우
# ADD ./pyenv.sh pyenv.sh
# RUN chmod 775 pyenv.sh && ./pyenv.sh
# ENV PYENV_ROOT '/.pyenv'
# ENV PATH ${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}

# jupyter 설정 폴더 추가
RUN mkdir -p /root/.jupyter

# 사용자 추가
#ENV USERS {사용자1 사용자2}
#RUN bash -c 'for user in ${USERS[*]}; do echo ${user}; useradd ${user} -ms /bin/bash && echo ${user}:${user} | chpasswd && echo "${user} ALL=(ALL:ALL) ALL" >> /etc/sudoers; done'

#서비스 시작
RUN apt upgrade -y && apt-get autoremove -y && apt-get autoclean -y && rm -rf /var/lib/apt/lists/* && apt update
CMD ["/bin/bash"]
# systemd 사용 시
#CMD ["/bin/init"]
