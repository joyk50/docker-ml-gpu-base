#!/bin/bash
# 설치할 파이썬 버전, 기본 설치 위치 설정
export PYTHON_VERSION=3.7.7
export PYENV_ROOT="/.pyenv"

# 필요 패키지 설치
DISTRO=$(. /etc/os-release;echo $ID)
echo $DISTRO

if [ "${DISTRO}" = centos ]; then
    sudo curl -sL https://rpm.nodesource.com/setup_12.x | bash -
    sudo yum clean all && sudo yum makecache fast
    sudo yum install -y zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel libffi-devel openssl-devel xz xz-devel curl git gcc-c++ make
    sudo yum install -y nodejs yarn
    sudo npm install -g eslint configurable-http-proxy
elif [ "${DISTRO}" = ubuntu ]; then
    sudo curl -sL https://deb.nodesource.com/setup_12.x | bash -
    sudo apt clean && sudo apt update
    sudo apt update && apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev libffi-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev g++ make
    sudo apt install -y nodejs yarn
    sudo npm install -g eslint configurable-http-proxy
else
    echo "현재 PYENV 설치 스크립트는 centos, ubuntu distro 만 지원합니다."
fi

if [ ! $(whoami) = 'root' ] ; then
    echo "PYENV 설치 및 환경 변수 설치로 인해 ROOT 권한이 요구됩니다."
    sudo -s $0
fi

# pyenv 설치 스크립트(인터넷 되야함)
curl https://pyenv.run | bash
sudo ln -s ${PYENV_ROOT} /root

# 환경 변수 등록 (서드파티 쉘 설치시에는 직접 추가해 줄 것, 주의 pyenv 설치 경로)
if  cat /etc/profile | grep "PYENV_ROOT" ; then
    echo "INFO: Already Set PYENV_ROOT: ${PYENV_ROOT}"
else
    echo export 'PYENV_ROOT="/.pyenv"' >> /etc/profile
    echo export 'PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"' >> /etc/profile
    echo 'eval "$(pyenv init -)"' >> /etc/profile
    echo export 'PYENV_ROOT="/.pyenv"' >> /etc/bashrc
    echo export 'PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"' >> /etc/bashrc
    echo 'eval "$(pyenv init -)"' >> /etc/bashrc
fi

# 파이썬 설치
source /etc/profile
pyenv install ${PYTHON_VERSION} && pyenv global ${PYTHON_VERSION}

# pip & 필요 패키지 설치
if [ -f get-pip.py ] ; then
    echo "get-pip.py has already downloaded."
else
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
fi
python get-pip.py && python3 get-pip.py
pip install -U setuptools
pip install -r requirements.txt
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter lab build

chmod -R 775 ${PYENV_ROOT}