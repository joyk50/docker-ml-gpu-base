#!/bin/bash
# 인증서 설정 (자체적으로 구성하는 스크립트 작동 안함)
if [ ! -z "$1" ] && [ "$1"=="office.leevi.co.kr" ]; then
    if [ "$HOSTNAME" != leevi-ML ]; then
        if [ -d /etc/letsencrypt/live ]; then
            rm -rf /etc/letsencrypt/live
            mkdir -p /etc/letsencrypt/live
        fi
        echo "leevi.co.kr SSL 파일 복사"
        rsync -vruh -zz -L --progress -e "ssh -p 32222" leevi@office.leevi.co.kr:/etc/letsencrypt/live/* /etc/letsencrypt/live
        chmod -R 777 /etc/letsencrypt/live
    else
        echo '기존 호스트 인증서 사용'
    fi
else
    set -e
    {
        echo 'SSL 인증서 설정: 80, 433 포트 오픈 되있어야 함.'
        LETSENCRYPT_DIR="/etc/letsencrypt"
        BACKUPS_DIR="/var/lib/letsencrypt"
        WILDCARD_SERVER="https://acme-v02.api.letsencrypt.org/directory"

        RUN_PARAMS="--net host -v ${LETSENCRYPT_DIR}:${LETSENCRYPT_DIR} -v ${BACKUPS_DIR}:${BACKUPS_DIR}"
        docker run -it --rm --name certbot ${RUN_PARAMS} \
        certbot/certbot renew --dry-run --no-self-upgrade --force-renewal
    } || {
        echo "SSL 인증서 설정 실패, 컨테이너 안의 /root/.jupyter/jupyterhub-config.py의 \\
            c.JupyterHub.ssl_cert = '' , c.JupyterHub.ssl_key = '' 로 바꾸시오."
    }
fi
