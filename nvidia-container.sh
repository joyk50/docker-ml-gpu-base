#!/bin/bash
export DISTRO=$(. /etc/os-release;echo $ID)
export ARCH=x86_64
echo $DISTRO

# cuda, cudann 설치 (nvidia 드라이버는 배포판에 따라 설치 방법 적용)
# Tensorflow Requirements 
export CUDA_VERSION=10.1
export CUDNN_VERSION=7.6.5.32-1
if [ "${DISTRO}" = centos ]; then
    DISTRO_VER='rhel7'
    sudo rpm -ivh https://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO_VER}/${ARCH}/nvidia-machine-learning-repo-${DISTRO_VER}-1.0.0-1.${ARCH}.rpm
    #sudo yum install -y cuda-${CUDA_VERSION//./-} libcudnn${CUDNN_VERSION:0:1}-${CUDNN_VERSION}.cuda${CUDA_VERSION} libcudnn${CUDNN_VERSION:0:1}-devel-${CUDNN_VERSION}.cuda${CUDA_VERSION} 하향지원 되는거 확인했는데 문제 생기는 경우
    sudo yum install -y kernel-devel-$(uname -r) kernel-headers-$(uname -r)
    sudo yum install -y cuda-${CUDA_VERSION//./-} libcudnn${CUDNN_VERSION:0:1} libcudnn${CUDNN_VERSION:0:1}-devel
    sudo rmdir /usr/local/cuda 
    sudo alternatives --install /usr/local/cuda cuda /usr/local/cuda-10.1 2
    echo 2 | sudo alternatives --config cuda
elif [ "${DISTRO}" = ubuntu ] || [ "${DISTRO}" = elementary ]; then
    DISTRO_VER='ubuntu1804'
    sudo apt-get install linux-headers-$(uname -r)
    wget https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO_VER}/${ARCH}/cuda-${DISTRO_VER}.pin
    sudo mv cuda-${DISTRO_VER}.pin /etc/apt/preferences.d/cuda-repository-pin-600
    sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO_VER}/${ARCH}/7fa2af80.pub
    sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/${DISTRO_VER}/${ARCH}/ /"
    wget https://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO_VER}/${ARCH}/nvidia-machine-learning-repo-${DISTRO_VER}_1.0.0-1_amd64.deb
    sudo dpkg -i nvidia-machine-learning-repo-*
    sudo apt update
    #sudo apt install --allow-downgrades --no-install-recommends -y cuda-${CUDA_VERSION//./-} libcudnn${CUDNN_VERSION:0:1}=${CUDNN_VERSION}+cuda${CUDA_VERSION} libcudnn${CUDNN_VERSION:0:1}-dev=${CUDNN_VERSION}+cuda${CUDA_VERSION}
    sudo apt install -y linux-headers-$(uname -r)
    sudo apt install -y cuda-${CUDA_VERSION//./-} libcudnn${CUDNN_VERSION:0:1} libcudnn${CUDNN_VERSION:0:1}-dev
    sudo rmdir /usr/local/cuda 
    sudo update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-10.1 2
    echo 2 | sudo update-alternatives --config cuda
elif [ "${DISTRO}" = manjaro ] || [ "${DISTRO}" = archlinux ]; then
    echo $(uname -r)
    sudo pacman -S linux-headers
fi

# LD_LIBRARY_PATH 설정
if  cat /etc/profile | grep "CUDA_PATH" ; then
    echo "INFO: Already Set CUDA_PATH: ${CUDA_PATH}"
else
    echo export 'CUDA_PATH="/usr/local/cuda"' >> /etc/profile
    echo export 'PATH="${CUDA_PATH}/bin:${PATH}"' >> /etc/profile
    echo export 'LD_LIBRARY_PATH="${CUDA_PATH}/lib64:${LD_LIBRARY_PATH}"' >> /etc/profile
    echo export 'CUDA_PATH="/usr/local/cuda"' >> /etc/bashrc
    echo export 'PATH="${CUDA_PATH}/bin:${PATH}"' >> /etc/bashrc
    echo export 'LD_LIBRARY_PATH="${CUDA_PATH}/lib64:${LD_LIBRARY_PATH}"' >> /etc/bashrc
fi

# nvidia-container-runtime 설치
DISTRO=$(. /etc/os-release;echo $ID)
if [ "${DISTRO}" = centos ]; then
    DISTRO_VER=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-container-runtime/${DISTRO_VER}/nvidia-container-runtime.repo | sudo tee /etc/yum.repos.d/nvidia-container-runtime.repo
    sudo yum install -y nvidia-container-runtime
    sudo systemctl restart docker
elif [ "${DISTRO}" = ubuntu ] || [ "${DISTRO}" = elementary ]; then
    distribution="ubuntu18.04"
    curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
    curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
    sudo apt-get update && sudo apt-get install nvidia-container-runtime
    sudo systemctl restart docker
elif [ "${DISTRO}" = manjaro ]; then
    DISTRO=$(. /etc/os-release;echo $ID$VERSION_ID)
    yay -Sy libnvidia-container nvidia-container-runtime
    sudo systemctl restart docker
else
    echo "현재 설치 스크립트는 Ubuntu, elemantary, manjaro distro 만 지원합니다."
fi
