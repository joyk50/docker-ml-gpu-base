#!/bin/bash
if [ ! $(whoami) = 'root' ] ; then
    echo "PYENV 설치 및 환경 변수 설치로 인해 ROOT 권한이 요구됩니다."
    exit 100
fi

# gcc 업데이트가 필요함
sudo yum install -y centos-release-scl
sudo yum install -y devtoolset-8
. /opt/rh/devtoolset-8/enable

# REPO 이용 (의존성 해결에서 안됨 강제로 설치하는 옵션 넣으면 될듯)
# rpm --import http://mirror.ghettoforge.org/distributions/gf/RPM-GPG-KEY-gf.el7
# rpm -ivh http://mirror.ghettoforge.org/distributions/gf/gf-release-latest.gf.el7.noarch.rpm
# yum --enablerepo=gf-testing install glibc231 glibc231-langpack glibc231-common

# RPM 직접 설치 (설치는 됨. 실행은 안됨 기존 ld config가 충돌 나는 듯함)
# ARCH=x86_64
# GLIBC_VERSION=2.27
# GLIBC_RELEASE=3.gf.el7
#echo ${GLIBC_RELEASE} | tr '.' '\n' | tail -1
# REPO_URL="http://mirror.ghettoforge.org/distributions/gf/el/7/testing/${ARCH}"
# declare -a FILES=("glibc${GLIBC_VERSION//./}-common-${GLIBC_VERSION}-${GLIBC_RELEASE}.${ARCH}.rpm" \
# "glibc${GLIBC_VERSION//./}-devel-${GLIBC_VERSION}-${GLIBC_RELEASE}.${ARCH}.rpm" \
# "glibc${GLIBC_VERSION//./}-headers-${GLIBC_VERSION}-${GLIBC_RELEASE}.${ARCH}.rpm" \
# "glibc${GLIBC_VERSION//./}-all-langpacks-${GLIBC_VERSION}-${GLIBC_RELEASE}.${ARCH}.rpm" \
# "glibc${GLIBC_VERSION//./}-${GLIBC_VERSION}-${GLIBC_RELEASE}.${ARCH}.rpm")
# for file in ${FILES[@]} ; do 
#     if [ ! -f  ${file} ] ; then
#         wget ${REPO_URL}/${file}
#     fi
# done
# rpm -Uvh --force --nodeps glibc*.rpm

# 소스 빌드 (빌드까지 되는데 core dump 남)
# GLIBC_VERSION=2.27
# if [ ! -f glibc-${GLIBC_VERSION}.tar.gz ]; then
#     wget http://ftp.gnu.org/gnu/glibc/glibc-${GLIBC_VERSION}.tar.gz
# fi
# tar zxvf glibc-*.tar.gz
# cd glibc-*
# mkdir build; cd build;
# unset LD_LIBRARY_PATH
# ../configure --prefix=/opt/glibc-${GLIBC_VERSION}
# make -j4
# make install
# if cat /etc/profile | grep glibc-${GLIBC_VERSION} ; then
#     echo "이미 경로에 추가됨"
# else
#     echo 'export GLIBC_VERSION=2.27' >> /etc/profile
#     echo 'export LD_LIBRARY_PATH="/opt/glibc-"${GLIBC_VERSION}"/lib:${LD_LIBRARY_PATH}"' >> /etc/profile
# fi
# . /etc/profile