import os
print(f'PATH: {os.environ["PATH"]}')
print(f'LD_LIBRARY_PATH: {os.environ["LD_LIBRARY_PATH"]}')

import tensorflow as tf
print(tf.__version__)
tf.test.is_gpu_available()
#tf.config.list_physical_devices('GPU')