#! /bin/bash
HERE="$( cd $(dirname $0) ; pwd -P )"
source ${HERE}/.env

PRIVILEGED="--privileged"
SYSTEMD="--mount type=tmpfs,destination=/app -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v /run:/run"
VPN_PARAM="--cap-add=NET_ADMIN --device=/dev/net/tun:/dev/net/tun"
MOUNT_USERS="-v ${PASSWD} -v ${GROUP} -v ${SHADOW} -v ${SUDOERS}"

RUN_PARAMS="--net host \
    ${PRIVILEGED} -v ${TIMESYNC} -v ${TIMEZONE} -v ${FONTS} -v ${SSL_CERT} \
    ${MOUNT_USERS} ${MOUNT_LOCALS} \
    -v ${HOME_DIR}:${HOME_DIR} -v ${MNT_DIR}:${MNT_DIR}"

DISTRO=$(. /etc/os-release;echo $ID)
if [ -z "$1" ]; then
    echo "${DISTRO} -  컨테이너 시작"
    if [ "${DISTRO}" = centos ]; then
        {
            $0 start
        } || {
            set -e
        }
    elif [ "${DISTRO}" = ubuntu ] || [ "${DISTRO}" = elementary ]; then
        $0 debian
    fi
fi
case "$1" in
compose)
    echo "docker-compose 설치: ${DOCKER_COMPOSE_VERSION}"
    if [[ $2 = "force" ]] ; then
        echo "Reinstall docker-compose. Must need root permission."
        {
            sudo rm -f ${DOCKER_COMPOSE_PATH}
            echo "${DOCKER_COMPOSE_PATH} was removed."
            sudo $0 compose
        } || {
            echo "Failed to remove ${DOCKER_COMPOSE_PATH}"
        }
    elif [ -f ${DOCKER_COMPOSE_PATH} ] ; then
        echo "${DOCKER_COMPOSE_PATH} has already downloaded."
    else
        echo "Install docker-compose. Must need root permission."
        {
            echo "Try to download docker-compose binary."
            sudo curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o ${DOCKER_COMPOSE_PATH}
        } || {
            echo "Failed. Copy the binary to ${DOCKER_COMPOSE_PATH}"
            sudo cp -f $HERE/../docker/docker-compose ${DOCKER_COMPOSE_PATH}
        }
        sudo chmod +x ${DOCKER_COMPOSE_PATH}
    fi
    ;;
build)
   {
    if ! [ -f ${DOCKER_COMPOSE_PATH} ] ; then
        $0 compose
    fi
    } || {
        echo "docker-compose already installed."
    }
    echo "이미지 빌드 생성 시작: ${FROM_IMAGE} -> ${IMAGE}"
        docker-compose --env-file ${HERE}/.env -f ${HERE}/docker-compose.yml build --force-rm --parallel --pull
    
    # 컨테이너 내외부 공유 라이브러리 맞출 생각이면 복사 할것 (centos7 glibc2.27 이상 업데이트 필요)
    # if [ ! -d "${PYENV_ROOT}" ]; then
    #     echo 'PYENV 미설치로 컨테이너 설치 내용 복사'
    #     docker run -itd --rm --name ${CONTAINER_NAME}_init ${IMAGE}
    #     sudo docker cp ${CONTAINER_NAME}_init:${PYENV_ROOT}/ ${PYENV_ROOT}
    #     sudo chmod -R 775 ${PYENV_ROOT}
    #     docker rm -f ${CONTAINER_NAME}_init
    # fi

    # 기본 파이썬 버전 올라가면 맞춰줄 것
    # echo '컨테이너 설치 패키지 복사' 
    # docker run -itd --rm --name ${CONTAINER_NAME}_init ${IMAGE}
    # sudo docker cp ${CONTAINER_NAME}_init:${DIR_PYTHON_CONTAINER}/dist-packages/ ${HOST_PYTHON_3_6_PATH}/site-packages
    # sudo chmod -R 775 ${DIR_PYTHON_HOST}
    # docker rm -f ${CONTAINER_NAME}_init
    
    if cat /etc/passwd | grep "_apt" ; then
        echo "이미 설정됨: "$(cat /etc/passwd | grep "_apt")
    else
        echo "_apt:x:104:65533::/noexistent:/sbin/nologin" >> /etc/passwd
    fi
    echo '이미지 빌드 완료'
    # $0 certify
    ;;
certify)
    if [ ! -z $DOMAIN_NAME ]; then
        echo "$DOMAIN_NAME 인증서 복사"
        ${HERE}/ssl.sh ${DOMAIN_NAME}
    else 
        ${HERE}/ssl.sh
    fi
    ;;
bash)
    $0 stop
    echo ${IMAGE} --> ${CONTAINER_NAME}
    docker-compose run ${SERVICE_NAME} /bin/bash
    ;;
lab)
    $0 stop
    echo ${IMAGE} --> ${CONTAINER_NAME}
    docker-compose run ${SERVICE_NAME} bash -c "jupyter lab --ip 0.0.0.0 --port ${JUPYTER_PORT}"
    ;;
start)
    $0 stop
    echo "Running Container: ${IMAGE}"
    echo "Python Version: ${PYTHON_VERSION}"
    echo "CUDA Version: ${CUDA_VERSION}"
    docker-compose -f ${DIR_SOURCE_HOST}/docker-compose.yml up
    ;;
debian)
    $0 stop
    echo "Running Container: ${IMAGE}"
    PACKAGES_DIR_HOST=/usr/local/lib/python3.6/dist-packages
    docker-compose -f ${DIR_SOURCE_HOST}/docker-compose.yml up
    ;;
stop)
    echo "Stopping Container:"
    docker rm -f ${SERVICE_NAME}
    ;;
restart)
    docker commit ${SERVICE_NAME} ${IMAGE}
    echo "${SERVICE_NAME} --> ${IMAGE} 커밋 완료"
    docker restart ${SERVICE_NAME}
    ;;
disconnect)
    docker network disconnect --force bridge ${SERVICE_NAME}
    ;;
prune)
    docker rmi $(docker images -f "dangling=true" -q)
    ;;
esac
